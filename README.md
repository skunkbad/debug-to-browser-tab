# Debug to Browser Tab

This *[package](https://packagist.org/packages/skunkbad/debug-to-browser-tab)* allows for debugging PHP variables in a browser tab.

Through Symfony VarDumper, debugging data is written to a log file. Browsersync is set up to watch the log file, and when it does your browser tab will reload the contents of a PHP file that includes the log file.

If you don't have Gulp or Browsersync:

```shell
cd /your/project/path
npm install gulp-cli
npm install browser-sync
```


You need to integrate Browsersync. 
I use Gulp, so the following is integrated into my Gulpfile.js:

```javascript
// Change this to your domain name
const host = 'example.com';

// This is the relative path from this Gulpfile to dumpLog.txt
const base = 'vendor/src/';

// This is the full path from the website's doc root to dumpLog.txt
const vndr = 'path/to/' + base;

const gulp = require('gulp'); 
const bs   = require('browser-sync').create();

gulp.task('dumplog', function() {
    bs.init({
        files: [base + 'dumpLog.txt'],
        proxy: host + '/' + vndr + 'dumpLog.php?vndr=' + encodeURIComponent(vndr)
    });
});
```


Load the debugger in your PHP, right after including composer's autoloader:

```php
<?php
require 'vendor/autoload.php';
new \Skunkbad\Debugger\DebugToBrowserTab;
```


start Browsersync:

```shell
cd /your/project/path
gulp dumplog
```


While working in PHP, use the dump function to send debugging data to the browser tab:

```php
<?php
dump( $foo );
```
