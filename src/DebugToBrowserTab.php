<?php
/**
 * Debug to browser tab
 * ----------------------
 * Allows PHP debugging data to be sent to a browser tab via browser-sync
 *
 * @package   skunkbad/debug-to-browser-tab
 * @author    Robert B Gottier
 * @copyright (c) 2020, Robert B Gottier (https://brianswebdesign.com)
 * @license   BSD - http://www.opensource.org/licenses/BSD-3-Clause
 */

namespace Skunkbad\Debugger;

use Symfony\Component\VarDumper\VarDumper;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use Symfony\Component\VarDumper\Dumper\ServerDumper;

class DebugToBrowserTab {

	private $dumpToFile = TRUE;

	private $dumpLog = '';

	/**
	 * Class constructor
	 */
	public function __construct( $config = [] )
	{
		$this->dumpLog = __DIR__ . '/dumpLog.txt';

		$this->load_dumper();
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Load Symfony VarDumper unless there is already a global dump function
	 */
	public function load_dumper()
	{
		// Dump to file
		if( $this->dumpToFile )
		{
			VarDumper::setHandler(function ($var) {
				$cloner = new VarCloner();
				$dumper = ( ! defined('WP_CLI') && 'cli' === PHP_SAPI )
					? new CliDumper() 
					: new HtmlDumper();
				$dumper->setDisplayOptions([
					'maxDepth' => 12
				]);

				$output = '';

				$dumper->dump(
					$cloner->cloneVar($var), 
					function ($line, $depth) use (&$output) {
						if ($depth >= 0) {
							$output .= str_repeat('  ', $depth).$line."\n";
						}
					}
				);
				file_put_contents($this->dumpLog, $output, FILE_APPEND);
			});
		}

		// Else dump to console
		// Start console in terminal with command: ./vendor/bin/var-dump-server
		else
		{
			VarDumper::setHandler(function ($var) {
				$cloner = new VarCloner();
				$dumper = new ServerDumper('tcp://127.0.0.1:9912');     
				$dumper->dump($cloner->cloneVar($var));
			});
		}
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Log
	 */
	public function log( $val )
	{
		if( function_exists('dump') )
			dump( $val );
	}
	
	// -----------------------------------------------------------------------
	
	/**
	 * Clear the log file
	 */
	public function clear()
	{
		file_put_contents($this->dumpLog, '');
	}
	
	// -----------------------------------------------------------------------

}