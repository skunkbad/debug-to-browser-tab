<?php 
if( isset( $_GET['clr'] ) )
{
	file_put_contents('dumpLog.txt', '');
	die();
}
?>
<html>
<head>
<style>
*{
	box-sizing: border-box;
}
html, body{
	margin:0;
	padding:0;
}
html, body, article{
	background-color:#222;
}
#wrapper {
	height:100%;
    display:flex;
    flex-direction:column;
}
article{
	flex:1;
	width:100%;
	padding:0rem .5rem;
	overflow:auto;
}
footer{
	width:100%;
	background-color:#111;
	padding:.5rem;
	flex:none;
	text-align:right;
}
</style>
</head>
<body>
	<div id="wrapper">
		<article>
			<?php
			include './dumpLog.txt';
			?>
		</article>
		<footer>
			<button type="button" id="clearButton">Clear</button>
		</footer>
	</div>
	<script>
	document.getElementById('clearButton').addEventListener('click', function(e){
		e.preventDefault();
		fetch('/<?php echo $_GET['vndr']; ?>dumpLog.php?clr=1')
			.then(x => {window.location.reload()});
	});
	</script>
</body>
</html>

