const host = 'localhost.debugger';
const vndr = 'src/'

const gulp = require('gulp'); 
const bs   = require('browser-sync').create();

gulp.task('dumplog', function() {
    bs.init({
    	open: false, // Open up your own tab
        files: [vndr + 'dumpLog.txt'],
        proxy: host + '/' + vndr + 'dumpLog.php?vndr=' + encodeURIComponent(vndr)
    });
});
